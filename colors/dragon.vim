" Inspired by elflord
set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "dragon"

hi Normal                               ctermfg=Red            ctermbg=Black     guifg=#e82c02  guibg=#000000
" For some reason linking these doesn't work...
hi ColorColumn                          ctermfg=Red            ctermbg=Black     guifg=#e82c02  guibg=#000000
hi SignColumn                           ctermfg=Red            ctermbg=Black     guifg=#e82c02  guibg=#000000
hi FoldColumn                           ctermfg=Red            ctermbg=Black     guifg=#e82c02  guibg=#000000
hi Directory                            ctermfg=DarkRed        guifg=#bb0000
hi Constant             term=bold       ctermfg=Green          guifg=#4cd622  
hi Title                                ctermfg=DarkMagenta    guifg=#e60058
hi TabLine              term=NONE       ctermfg=Red            ctermbg=Black     guifg=#e82c02  guibg=#000000
hi TabLineSel           term=NONE       ctermfg=DarkBlue       ctermbg=Black     guifg=#000ad1  guibg=#000000
hi Special              term=bold       ctermfg=DarkMagenta    guifg=#e60058   
hi Statement            term=bold       ctermfg=DarkMagenta    guifg=#e60058
hi Identifier           term=bold       ctermfg=DarkYellow     guifg=#ffb20d
hi Function             term=bold       ctermfg=DarkYellow     guifg=#ffb20d
hi PreProc                              ctermfg=DarkCyan       guifg=#18b2d9
hi Operator                             ctermfg=White          guifg=#ffffff
hi Ignore                               ctermfg=Black          guifg=#000000
hi Type                                 ctermfg=Blue           guifg=#3c12e3
hi Comment              term=italic     ctermfg=DarkBlue       guifg=#000ad1
hi Error                                ctermbg=DarkMagenta    ctermfg=White     guibg=#e60058  guifg=#ffffff
hi MatchParen           term=bold       ctermbg=DarkMagenta    ctermfg=White     guibg=#e60058  guifg=#ffffff
hi ErrorMsg                             ctermbg=DarkMagenta    ctermfg=White     guibg=#e60058  guifg=#ffffff
hi Todo                                 ctermbg=DarkYellow     ctermfg=DarkRed   guibg=#ffb20d  guibg=#bb0000
hi CursorLine                           ctermbg=8              guibg=#250000
hi CursorColumn                         ctermbg=8              guibg=#250000
hi CursorLineNr         term=bold       ctermfg=DarkYellow     ctermbg=8         guifg=#ffb20d  guibg=#250000
hi Linenr                               ctermfg=7              guifg=#e8c4c4
hi StorageClass         term=bold       ctermfg=DarkGreen      guifg=#1aba37
hi Macro                term=bold       ctermfg=Yellow         guifg=#ffe922
hi Folded               term=bold       ctermfg=Red            ctermbg=Cyan      guifg=#e82c02  guibg=#00d1ce
" rust
hi rustCommentLineDoc   term=italic     ctermfg=DarkGreen      guifg=#1aba37
hi rustModPathSep       term=italic     ctermfg=Magenta        guifg=#ff0095
hi rustAssert           term=bold       ctermfg=Cyan           guifg=#00d1ce
hi link rustPanic       rustAssert
hi link rustMacro       Macro
hi link rustDerive      Comment
hi link rustAttribute   Comment

hi link String          Constant
hi link Character       Constant
hi link Number          Constant
hi link Boolean         Constant
hi link Float           Constant
hi link Conditional     Statement
hi link Repeat          Statement
hi link Label           Statement
hi link Keyword         Statement
hi link Exception       Statement
hi link Include         PreProc
hi link Define          PreProc
hi link PreCondit       PreProc
hi link EndOfBuffer     Function
hi link Macro           Function
hi link Structure       Type
hi link Typedef         Type
hi link Tag             Special
hi link SpecialChar     Special
hi link Delimiter       Special
hi link SpecialComment  Special
hi link Debug           Special
hi link CursorColumn    CursorLine
hi link Nontext         Normal
