set nu
set termguicolors
set cursorline cursorcolumn
set title
set splitbelow
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab autoindent
set laststatus=2
set statusline=[%n]%{Colorscheme()}\|%t%m%r%<%=%c\|%l/%L%y
set showtabline=2
setlocal foldmethod=syntax
set foldnestmax=1
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
colo dragon

fun! Colorscheme()
    if exists("g:colors_name") | return g:colors_name | else | return "-" | endif
endfun
" switch tabs
nnoremap H gT
nnoremap L gt
